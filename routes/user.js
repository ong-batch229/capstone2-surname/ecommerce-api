const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
const auth = require("../auth")

router.post('/register', (req, res) => {
	userControllers.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController))
})

router.post('/login', (req, res) => {
	userControllers.login(req.body)
		.then(resultFromController => res.send(resultFromController))
})

router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id, 
		reqBody: req.body
	};
	if(userData.isAdmin == false) {
		userControllers.userCheckout(data).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("No authorization!")
	}
});

router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
