const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === true) {
		productControllers.createProduct(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not an admin!");
	}
});


router.get("/all", (req, res) => {
	productControllers.getAllProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

router.get("/:productId", (req, res) => {
	productControllers.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

router.put("/:productId/update/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === true) {
		productControllers.updateProductInfo(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not an admin!")
	}
});

router.put("/:productId/archive/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === true) {
		productControllers.archiveProduct(req.params).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not an admin!")
	}
});

module.exports = router;