const Product = require("../models/Product")

module.exports.createProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newProduct.save().then((product, error) => {
		if(error) {
			return false;
		} 
		else {
			return "Successfully Created!";
		}
	});
};

module.exports.getAllProducts = () => {
	return Product.find({}).then(product => {
		return product;
	});
};

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(product => {
		return product;
	});
};

module.exports.updateProductInfo = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} 
		else {
			return "Product Updated!";
		}
	});
};

module.exports.archiveProduct = (reqParams) => {
	let archivedCourse = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, archivedCourse).then((product, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	});
};
