const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody, res) => {
    return User.find({ email: reqBody.email }).then(result => {
        if(result.length > 0) {
            return "Already Registered!"
        } else {
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10) 
            })
            return newUser.save().then((user, error) => {
                if(error) {
                    return false
                } else {
                    return "Registered Seccessfully!"
                }
            })
        }
    })
}


module.exports.login = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if(result == null) {
            return "Email does not exist"
        } else {
            const match = bcrypt.compareSync(reqBody.password, result.password)

            if(!match) {
                return false
            } else {
                return {access: auth.createAccessToken(result)}
            }
        }
    })
}

module.exports.userCheckout = (data) => {
	return Product.findById(data.reqBody.productId).then(product => {
		if(product == null) {
			return "The product is not available!"
		}
		else {
			let newOrder = new Order({
				userId: data.userId,
				products: {
					productName: product.name,
					quantity: data.reqBody.quantity
				},
				totalAmount: product.price * data.reqBody.quantity
			});
			return newOrder.save().then((order, error) => {

				if(error) {
					console.log(error);
					return false
				}
				else {
					return "Success!";
				}
			});
		};
	});
};

module.exports.getProfile = (data) => {
	console.log(data)
	return User.findById(data.userId).then(result => {
		console.log(result);
		result.password = "";
		return result;
	});
}
